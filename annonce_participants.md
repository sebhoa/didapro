**DIDAPRO 9**<br>
**18 mai 2022, Le Mans**

# Mon premier site web en MkDocs

Dans cet atelier, vous allez découvrir en faisant... il vous faudra donc :

- un ordinateur portable pouvant se connecter au wifi de la salle ;
- un navigateur et un éditeur de texte ;
- un peu de contenu (pour éviter de remplir les paragraphes de _Le lorem ipsum est_) ; celui proposé en exemple lors de l'atelier sera une page sur le chiffrement de César : [sebhoa.gitlab.io/didapro/](https://sebhoa.gitlab.io/didapro/).

À bientôt,

Fabrice et Sébastien