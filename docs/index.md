# Accueil


!!! Warning "À l'attention des participant.e.s"
    
    Dans cette première page, vous pouvez effectuer la partie : _Mise en route_ comme préalable à l'atelier. 
    
    Les autres pages serviront pendant l'atelier, le 18 mai prochain. Rassurez-vous il s'agit bien d'une initiation à `mkdocs-material` et non au chiffrement de César :wink:
    
    Cet exemple servira de support pour une introduction progressive de quelques unes de fonctionnalités de l'outil. Son contenu peut vous guider pour choisir votre propre contenu, celui qui vous servira le jour de l'atelier pour la mise en pratique.


## Mise en route
1. Installation de mkdocs

    ```bash
    pip3 install mkdocs-material
    ```

2. Création nouveau site

    ```bash
    cd /le/repertoire/racine/du/projet
    mkdocs new .
    ```

3. Éditer le site 

    1. le fichier `mkdocs.yml`
    2. la page d'accueil : `docs/index.md`

4. Ajout du thème `material` 

    Dans `mkdocs.yml` ajouter les lignes suivantes :

    ```yaml
    theme:
        name: material
    ```

5. Visualiser le site

    ```bash
    mkdocs serve
    ```
    Puis ouvrir un navigateur et y mettre l'URL `localhost:8000` ou `127.0.0.1:8000`

## S'initier en construisant sa première page

L'atelier s'organise en plusieurs étapes chacune alternant deux phases.

* Étape 1 : fonctionnalités de base de Markdown  (titres, liens, mise en forme de texte, listes).
* Étape 2 : autres fonctionnalités (insertion de code, images et citations).
* Étape 3 : fonctionnalités supplémentaires du [thème mkdocs-material](https://squidfunk.github.io/mkdocs-material/) (icônes et emoji, maths, admonitions, onglets).
* Étape 4 : personnalisation (navigation, css propre).
* Étape 5 : mise en ligne et hébergement du site.


Les deux phases sont :

1. La présentation des fonctionnalités de MkDocs. L'exemple de la construction de ce site sur le chiffrement de César servira de fil rouge.
2. La mise en œuvre par les participants sur un exemple de leur choix. 

