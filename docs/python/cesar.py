from matplotlib.pyplot import text


def chiffre(lettre,cle):
    code_lettre = ord(lettre)-ord("A")
    code_lettre = (code_lettre+cle) %26 + ord("A")
    return chr(code_lettre)
    

def chiffre_cesar(texte, cle):
    texte_chiffre = ""
    for c in texte:
        if "A" <= c <= "Z":
            texte_chiffre += chiffre(c,cle)
        else:
            texte_chiffre += c
    return texte_chiffre

