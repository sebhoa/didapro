# Mettre en ligne

## Première possibilité 

On peut construire l'ensemble des pages qui seront alors disponible dans le répertoire `site`, par la commande suivante :

```bash
mkdocs build
```

Il faudra aussi songer à utiliser l'option suivante, à ajouter à votre fichier `mkdocs.yml` :

```yaml
use_directory_urls: false
```


## Deuxième possibilité

On peut aussi déposer l'ensemble des répertoires et fichiers sur une plateforme _git_ et utiliser le service d'intégration continue.

Il vous faudra alors ajouter un fichier `.yml` :

=== "Pour GitHub"

    À la racine de votre projet vous devez ajouter les répertoires et le fichier suivants :

    ```bash
    .github/workflows/ci.yml
    ```

    Avec le contenu ci-dessous :

    ```yaml
    name: ci 
    on:
        push:
            branches:
                - master 
                - main
    jobs:
        deploy:
            runs-on: ubuntu-latest
            steps:
            - uses: actions/checkout@v2
            - uses: actions/setup-python@v2
                with:
                python-version: 3.x
            - run: pip install mkdocs-material 
            - run: mkdocs gh-deploy --force
    ```

=== "Pour GitLab"

    À la racine de votre projet vous devez ajouter le fichier suivant :

    ```bash
    .gitlab-ci.yml
    ```

    Avec le contenu ci-dessous :

    ```yaml
    image: python:latest
    pages:
        stage: deploy
        only:
            - master 
            - main
        script:
            - pip install mkdocs-material
            - mkdocs build --site-dir public
        artifacts:
            paths:
            - public
    ```
