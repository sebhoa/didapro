# Code de César (V3)

> "Il y employait, pour les choses tout à fait secrètes, une espèce de chiffre qui en rendait le sens inintelligible (...)"

Suétone, *Vie des douze Césars, Livre I, paragraphe 56.*



Nous allons voir un algorithme de déchiffrage d'un [code de César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) fondé sur le calcul de fréquence d'apparitions de certaines lettres dans un texte.

## Principe du code de César


On choisit $c$  ($c \in \mathbb{N}, 1 \leq c \leq 25$) qui sert de clé et le principe de chiffrement est de décaler chaque lettre de $c$ emplacements dans l'alphabet.

!!! Exemple
    Par exemple, avec la clé $c=7$, le texte `DIDAPRO` devient : `KPKHWYV`. En effet :
    ```
    ABCDEFGHIJ ...
    ||||||||||
    HIJKLMNOPQ ...
    ```

## :fontawesome-regular-pen-to-square:   Exercices

1. Quelle est la clé qui chiffre B en E ? 

    ![Exo1](Caesar3.svg){ width=40% }

2. Avec une clé de 10, quel est le code de V ? 

3.  Déchiffrer "NG OCPU" avec la clé 2. 

## :material-language-python: Implémentation en Python

```python title="Fonctions Python"
def chiffre(lettre,cle):
    code_lettre = ord(lettre)-ord("A")
    code_lettre = (code_lettre+cle) %26 + ord("A")
    return chr(code_lettre)
    

def chiffre_cesar(texte, cle):
    texte_chiffre = ""
    for c in texte:
        if "A" <= c <= "Z":
            texte_chiffre += chiffre(c,cle)
        else:
            texte_chiffre += c
    return texte_chiffre
```

## Principe du déchiffrement par analyse fréquentielle

L'**analyse fréquentielle** consiste à mesurer la fréquence d'apparition d'une lettre dans une langue.

=== "Anglais"
    ![af_ang](af_en.svg){width=463px}

=== "Français"
    ![af_fr](af_fr.png)