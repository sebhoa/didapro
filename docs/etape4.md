
# Personnaliser

## La navigation

Vous trouverez le détail de ce que vous pouvez personnaliser sur la navigation au [chapitre _Setting up navigation_](https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/) de la documentation officielle.

Quatre zones de la page permettent de gérer la navigation :

- la colonne de gauche (en bleu sur le graphique ci-dessous), utilisée par défaut pour la navigation dans les différentes pages du site
- la colonne de droite (en mauve) fait apparaître par défaut le sommaire de la page courante
- l'entête du site (en vert), non utilisé par défaut
- le pied-de-page, pour un menu **non modifiable** de type _avancer_ / _reculer_

![menus](zones_menus.png)

### La navigation principale

Les pages du site sont constituées des fichiers _markdown_ se trouvant dans le dossier `docs` de l'arborescence du site. 

Ces fichiers peuvent se trouver à l'intérieur de sous-dossiers. Par défaut, il n'y a pas de limitation sur le nombre de sous niveaux présentés dans le menu.

Dans le menu, les pages sont présentées suivant l'ordre alphabétique des noms des fichiers, les répertoires apparaissant toujours après les fichiers.

#### Utiliser l'entête pour la navigation de niveau 1

Rajouter les lignes suivantes dans `mkdocs.yml` :

```yaml
features:
    - navigation.tabs
```

#### Choisir ses fichiers

Plutôt que de laisser _mkdocs_ utiliser tous les fichiers `.md` se trouvant dans le dossier `docs`, on peut choisir ses fichiers et créer son contenu manuellement :

```yaml
nav:
    - Ma première page: fichier.md
    - Une autre page: chemin/vers/autre_page.md
```

### Le sommaire

Le sommaire d'une page apparaît dans la colonne de droite. Par défaut, il est visible sur tous les niveaux de sous-titres. Néanmoins, il est possible de :

1. Inclure le sommaire dans la navigation principale dans la colonne de gauche :
    ```yaml title="mkdocs.yml"
    theme:
        features:
            - toc.integrate
    ```
2. Limiter les niveaux visibles (par exemple au sous-titres de niveau 3):
    ```yaml title="mkdocs.yml"
    - toc:
        toc_depth: 3
    ```
3. Masquer le sommaire : il faut ajouter les lignes suivantes au début du fichier markdown concerné
    ```md title="Méta-données"
    ---
    hide:
        - toc
    ---
    ```



