# Code de César (V1)

Nous allons voir un algorithme de déchiffrage d'un [code de César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) fondé sur le calcul de fréquence d'apparitions de certaines lettres dans un texte.

## Principe du code de César

On choisit c  (un entier de 1 à 25) qui sert de clé et le principe de chiffrement est de décaler chaque lettre de c emplacements dans l'alphabet.

Par exemple, avec la clé c=7, le texte "DIDAPRO" devient : "KPKHWYV"

## Exercices

1. Quelle est la clé qui chiffre B en E ? 

2. Avec une clé de 10, quel est le code de V ?

3. Déchiffrer "NG OCPU" avec 2 comme clé de déchiffrement.

## Implémentation en Python

*Ici on insère le code Python*

## Principe du déchiffrement par analyse fréquentielle

L'**analyse fréquentielle** consiste à mesurer la fréquence d'apparition d'une lettre dans une langue.

