# Aide mémoire 

## Fonctionnalités de base 

### :material-format-title: Les titres 

Les titres sont précédés d'un ou plusieurs caractères `#` (1 pour le titre principal à 6 pour le dernier niveau de sous-titre).

!!! example "Exemple"
    === "Fichier markdown"
        ```md
        # Titre

        ## Sous-titre

        ...

        ###### Quelqu'un utilise ce 6e niveau ?
        ```
    === "Rendu"
        <h1>Titre</h1>

        <h2>Sous-titre</h2>
        ...

        <h6>Quelqu'un utilise ce 6e niveau ?</h6>

### :material-format-font: Mise en forme du texte

L'_emphase faible_ s'obtient en entourant le texte concerné par des `*` ou des `_`, l'**emphase forte** par des `**` ou `__`.

!!! example "Exemple"
    === "Fichier markdown"
        ```md
        Un exemple d'emphase **forte** et d'emphase *faible*. Et on peut **_combiner_** tout ça.
        ```
    === "Rendu"
        Un exemple d'emphase **forte** et d'emphase *faible*. Et on peut **_combiner_** tout ça.

### :material-link: Lien hypertexte

Un lien s'obtient à l'aide de la syntaxe `[texte](url)`. On peut aussi effectuer un lien sur un autre objet que du texte (une image par exemple).

!!! example "Exemple"
    === "Fichier markdown"
        ```md
        La conférence [didapro 9](https://www.didapro.org/9/) se déroule du 18 au 20 mai 2022.
        ```
    === "Rendu"
        La conférence [didapro 9](https://www.didapro.org/9/) se déroule du 18 au 20 mai 2022.

### :octicons-list-unordered-16: Listes

Les listes à puces sont précédées d'une ligne vide, puis chaque élément de la liste commence sur nouvelle ligne et  est précédé de `* `. Plusieurs niveaux de listes peuvent être imbriquées, les listes numérotées s'obtiennent en remplaçant `*` par `1.` 

!!! example "Exemple"
    === "Fichier markdown"
        ```md
        En markdown, on peut facilement :

        * structurer son document avec des titres,
        * ajouter de l'emphase au texte,
        * insérer des liens,
        * faire des listes à puces 
        ```
    === "Rendu"
        En markdown, on peut facilement :

        * structurer son document avec des titres,
        * ajouter de l'emphase au texte,
        * insérer des liens,
        * faire des listes à puces.


## Autres fonctionnalités 


### :octicons-image-16: Image

Une image s'obtient avec la syntaxe `![texte alternatif](url)`. L'image peut être en local ou sur le web.

!!! example "Exemple"
    === "Fichier markdown"
        ```md
        ![Buste de César](cesar.png)        
        ```
    === "Rendu"
        ![Buste de César](cesar.png)

        
### :material-language-python: Insertion de code

#### Dans la ligne

On peut insérer du code dans la ligne de son paragraphe en délimitant la portion de texte avec des _blackquote_ :

!!! example "Exemple"

    === "Source markdown"
        ```md
        En Python, `ord(car)` renvoie le code ASCII du caractère `car`.
        ```

    === "Rendu"
        En Python, `ord(car)` renvoie le code ASCII du caractère `car`.

#### Comme bloc indépendant

Pour créer un bloc de code indépendant, il faut mettre des triple _backquote_, suivi, éventuellement du nom du langage pour obtenir la bonne colorisation syntaxique :

!!! example "Exemples"

    === "Python"

        Source markdown :

        ```md
            ```python
            def chiffre(lettre,cle):
                code_lettre = ord(lettre)-ord("A")
                code_lettre = (code_lettre+cle) %26 + ord("A")
                return chr(code_lettre) 
            ```
        ```

        Résultat obtenu :

        ```python
        def chiffre(lettre,cle):
            code_lettre = ord(lettre)-ord("A")
            code_lettre = (code_lettre+cle) %26 + ord("A")
            return chr(code_lettre) 
        ```

    === "SQL"

        Source markdown (on peut même ajouter un titre):

        ```md
            ```sql title="SQL -- extrait corrigé du BAC"
            SELECT titre, id_rea
            FROM realisation
            WHERE annee > 2020;
            ```
        ```

        Résultat obtenu :

        ```sql title="SQL -- extrait corrigé du BAC"
        SELECT titre, id_rea
        FROM realisation
        WHERE annee > 2020;
        ```

    === "HTML"

        Source markdown :

        ```md
          ```html
          <h1>Chiffrement de Caesar</h1>
          <p>Bienvenue dans cet atelier de DIDAPRO</p>
          ```
        ```

        Résultat obtenu :

        ```html
        <h1>Chiffrement de Caesar</h1>
        <p>Bienvenue dans cet atelier de DIDAPRO</p>
        ```


### :material-format-quote-open: Citation

On introduit une citation par le caractère `>` :

!!! example "Exemple"

    === "Source markdown"

        ```md
        > Nous avons un recueil des lettres de J. César à C. Oppius et Balbus Cornelius, chargés du soin de ses affaires en son absence. Dans ces lettres, on trouve, en certains endroits, des fragments de syllabes sans liaison, caractères isolés, qu'on croirait jetés au hasard : il est impossible d'en former aucun mot. C'était un stratagème dont ils étaient convenus entre eux : sur le papier une lettre prenait la place et le nom d'une autre ; mais le lecteur restituait à chacune son nom et sa signification

        --- Aulu-Gelle, **_Nuits attiques_**, _livre XVII, 9_
        ```

    === "Rendu"

        > Nous avons un recueil des lettres de J. César à C. Oppius et Balbus Cornelius, chargés du soin de ses affaires en son absence. Dans ces lettres, on trouve, en certains endroits, des fragments de syllabes sans liaison, caractères isolés, qu'on croirait jetés au hasard : il est impossible d'en former aucun mot. C'était un stratagème dont ils étaient convenus entre eux : sur le papier une lettre prenait la place et le nom d'une autre ; mais le lecteur restituait à chacune son nom et sa signification

        --- Aulu-Gelle, **_Nuits attiques_**, _livre XVII, 9_

On peut aussi utiliser une admonition (voir le chapitre suivant) :

!!! quote "Mark Twain"

    If you tell the truth, you don't have to remember anything.


## Fonctionnalités du thème Material

### :octicons-smiley-16: Icônes et emoji

Le thème Material offre de nombreuses icônes et émoticônes. Pour cela, il  faudra :

1. Ajouter cette ligne au fichier `docs.yml` :
   ```yaml title="mkdocs.yml"
   markdown_extensions:
   - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
   ```
2. Ajouter le code de l'émoji ou de l'icône :

    !!! Exemple "Un émoji"

        === "Source Markdown"

            ```md
            Logo Python : :material-language-python:
            ```

            Astuce : [Outil de recherche d'émojis](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/) du site officiel du thème Material. 

        === "Rendu"

            Logo Python : :material-language-python:


### :material-square-root: Formule de maths

On peut insérer des formules mathématiques en utilisant la syntaxe $\LaTeX$. Ces formules seront interprétées et affichées dans le navigateur via le module [MathJax](https://www.mathjax.org/). 

Pour cela il faut commencer par créer les répertoires et le fichier suivants dans le répertoire `docs` :

```bash
javascripts/mathjax.js
```

Avec le contenu :

```js title="javascripts/mathjax.js"
window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => { 
  MathJax.typesetPromise()
})
```

Et compléter le `mkdocs.yml` à l'aide ces quelques lignes :

```yaml title="mkdocs.yml"
markdown_extensions:
  - pymdownx.arithmatex:
      generic: true

extra_javascript:
  - javascripts/mathjax.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
```

Vous pourrez alors intégrer directement dans vos fichiers markdown des formules mathématiques :

!!! example "Exemple"

    Formule en ligne en délimitant les éléments par `$ ... $` ou `\( ... \)` :

    === "Source markdown"
        ```md
        Un entier $n$ qui vérifie : $\forall d\in\mathtt{N}, 1\lt d\lt n \Rightarrow n \operatorname{mod} d \neq 0$ est premier.
        ```

    === "Rendu"
        Un entier $n$ qui vérifie : $\forall d\in\mathtt{N}, 1\lt d\lt n \Rightarrow n \operatorname{mod} d \neq 0$ est premier.

!!! example "Exemple"

    Bloc mathématique en délimitant les éléments par `$$ ... $$` ou `\[ ... \]` :

    === "Source markdown"

        ```md
        Le développement limité de $e^x$ en $0$, à l'ordre $n$ est :

        $$e^x = 1 + \frac{x}{1!} + \frac{x²}{2!} + \cdots + \frac{x^n}{n!} + o(x^n)$$
        ```

    === "Rendu"
        Le développement limité de $e^x$ en $0$, à l'ordre $n$ est :

        $$e^x = 1 + \frac{x}{1!} + \frac{x²}{2!} + \cdots + \frac{x^n}{n!} + o(x^n)$$


### :material-tooltip-text-outline: Admonitions

L'admonition est un petit encadré pour donner une sémantique et un visuel particulier à une portion du document. Pour les utiliser il faudra ajouter les lignes suivantes à votre fichier `mkdocs.yml` :

```yaml title="mkdocs.yml"
markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
```

Puis l'admonition se compose :

1. d'une ligne d'entête qui commence par trois points d'exclamation et le nom de l'admonition
2. d'un contenu, qui se trouve en dessous de la ligne d'entête et tabulé 


```md title="Source markdown"
!!! example "Rendu"

    Ceci est un exemple de l'admonition example.    
```

!!! example "Rendu" 

    Ceci est un exemple de l'admonition example.    

Il existe plusieurs sortes d'admonitions que vous pourrez retrouver sur la page dédiée du site officiel : [les différentes admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/#supported-types)

On peut rendre une admonition _pliable_ en remplaçant les `!!!` par `???` pour que l'admonition soit repliée par défaut ou alors `???+` pour l'avoir dépliée.

```md title="Source markdown"
??? note "Repliée par défaut"

    Une note repliée par défaut.     
```

??? note "Repliée par défaut"

    Une note repliée par défaut.     

Et un avertissement :

```md title="Source markdown"
???+ warning "Avertissement, dépliée par défaut"

    **Attention** ceci est un avertissement.     
```

???+ warning "Avertissement, dépliée par défaut"

    **Attention** ceci est un avertissement.     



### :material-tab: Onglets

Les onglets permettent de présenter du contenu _empilé_ :

!!! example "Hello World!"

    === "Python"

        ```python
        print('Hello World!')
        ```

    === "Java"

        ```java
        public class HelloWorld 
        {                      
            public static void main(String[] args) 
            {                    
                System.out.println("Hello World!"); 
            }
        }
        ```

    === "C"

        ```c
        #include <stdio.h>
        int main(void)
        {
            printf("%s", "Hello World!\n");
            return 0;
        }
        ```

    === "Javascript"

        ```javascript
        <script>
        document.write('Hello World ! <br />');
        </script>
        ```

    === "Haskell"

        ```haskell
        main = putStrLn "Hello World!"
        ```

    === "OCaml"

        ```ocaml
        print_string "Hello World!\n";;
        ```

    === "Pascal"

        ```pascal
        program hello;
        begin
            writeln('Hello World!');
        end.
        ```

## Conclusion

Retrouvez toutes les références sur la [page officielle de material](https://squidfunk.github.io/mkdocs-material/reference/).